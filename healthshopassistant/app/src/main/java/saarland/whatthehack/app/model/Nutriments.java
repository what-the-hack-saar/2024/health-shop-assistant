package saarland.whatthehack.app.model;



import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Data;

@Data
public class Nutriments {

    private float calcium;

    
    private float calciumValue;

    
    private float calcium100G;

    
    private float calciumServing;

    
    private String calciumUnit;

    private float carbohydrates;

    
    private float carbohydratesValue;

    
    private float carbohydrates100G;

    
    private float carbohydratesServing;

    
    private String carbohydratesUnit;

    
    private float carbonFootprintFromKnownIngredientsProduct;

    
    private float carbonFootprintFromKnownIngredients100G;

    
    private float carbonFootprintFromKnownIngredientsServing;

    private float cholesterol;

    
    private float cholesterolValue;

    
    private float cholesterol100G;

    
    private float cholesterolServing;

    
    private String cholesterolUnit;

    private int energy;

    
    private int energyKcal;

    
    private int energyKj;

    
    private int energyValue;

    
    private int energyKcalValue;

    
    private int energyKjValue;

    
    private int energy100G;

    
    private int energyKcal100G;

    
    private int energyKj100G;

    
    private int energyServing;

    
    private int energyKcalServing;

    
    private int energyKjServing;

    
    private String energyUnit;

    
    private String energyKcalUnit;

    
    private String energyKjUnit;

    private float fat;

    
    private float fatValue;

    
    private float fat100G;

    
    private float fatServing;

    
    private String fatUnit;

    private float fiber;

    
    private float fiberValue;

    
    private float fiber100G;

    
    private float fiberServing;

    
    private String fiberUnit;

    
    private float fruitsVegetablesNutsEstimateFromIngredients100G;

    private float iron;

    
    private float ironValue;

    
    private float iron100G;

    
    private float ironServing;

    
    private String ironUnit;

    
    private float novaGroup;

    
    private float novaGroup100G;

    
    private float novaGroupServing;

    private float proteins;

    
    private float proteinsValue;

    
    private float proteins100G;

    
    private float proteinsServing;

    
    private String proteinsUnit;

    private float salt;

    
    private float saltValue;

    
    private float salt100G;

    
    private float saltServing;

    
    private String saltUnit;

    
    private float saturatedFat;

    
    private float saturatedFatValue;

    
    private float saturatedFat100G;

    
    private float saturatedFatServing;

    
    private String saturatedFatUnit;

    private float sodium;

    
    private float sodiumValue;

    
    private float sodium100G;

    
    private float sodiumServing;

    
    private String sodiumUnit;

    private float sugars;

    
    private float sugarsValue;

    
    private float sugars100G;

    
    private float sugarsServing;

    
    private String sugarsUnit;

    
    private float transFat;

    
    private float transFatValue;

    
    private float transFat100G;

    
    private float transFatServing;

    
    private String transFatUnit;

    
    private float vitaminA;

    
    private float vitaminAValue;

    
    private float vitaminA100G;

    
    private float vitaminAServing;

    
    private String vitaminAUnit;

    
    private float vitaminC;

    
    private float vitaminCValue;

    
    private float vitaminC100G;

    
    private float vitaminCServing;

    
    private String vitaminCUnit;

    
    private float vitaminD;

    
    private float vitaminDValue;

    
    private float vitaminD100G;

    
    private float vitaminDServing;

    
    private String vitaminDUnit;

    Map<String, Object> other = new LinkedHashMap<>();

   
    void setDetail(String key, Object value) {
        other.put(key, value);
    }
}