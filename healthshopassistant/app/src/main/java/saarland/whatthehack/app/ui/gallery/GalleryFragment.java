package saarland.whatthehack.app.ui.gallery;

import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;


import java.util.Arrays;
import java.util.Objects;

import saarland.whatthehack.app.client.OpenFoodFactsApiLowLevelClient;
import saarland.whatthehack.app.databinding.FragmentGalleryBinding;
import saarland.whatthehack.app.model.ProductResponse;

public class GalleryFragment extends Fragment {

    private FragmentGalleryBinding binding;
    private static final String MAGGI = "4005500310105";
    private static final String SIMPLIV = "4260444963040";
    private static final String GUTFRIED = "4003171096175";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        GalleryViewModel galleryViewModel =
                new ViewModelProvider(this).get(GalleryViewModel.class);

        binding = FragmentGalleryBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final Button
                btnEanCode = binding.btnSeachEan;

        final EditText eanView = binding.eancode;
        final TextView textView = binding.editTextTextMultiLine;
        final TextView zutatenView = binding.txtZutaten;
        final TextView productnameView = binding.txtProductName;
        final TextView aufpassenView = binding.txtAufpassen;


        galleryViewModel.getText().observe(getViewLifecycleOwner(), textView::setText);
        galleryViewModel.getEanText().observe(getViewLifecycleOwner(),eanView::setText);
        galleryViewModel.getZutatenText().observe(getViewLifecycleOwner(),zutatenView::setText);
        galleryViewModel.getProduktText().observe(getViewLifecycleOwner(),productnameView::setText);
        galleryViewModel.getAufpassenText().observe(getViewLifecycleOwner(),aufpassenView::setText);

        btnEanCode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                OpenFoodFactsApiLowLevelClient wrapper = null;
                try {
                    wrapper = new OpenFoodFactsApiLowLevelClient();
                    ProductResponse productResponse = wrapper.fetchProductByCode(eanView.getText().toString());
                    textView.setText(productResponse.getProduct()
                            .getAllergens());
                    zutatenView.setText(TextUtils.join(",", Arrays.stream(productResponse.getProduct().getIngredients()).map(item->item.getText()).toArray(String[]::new)));
                    productnameView.setText(productResponse.getProduct().getProductName());
                    aufpassenView.setText("");
                    //playNeutral();
                    if ((eanView.getText().toString().equals(MAGGI))||(eanView.getText().toString().equals(SIMPLIV)))
                    {
                        setAufpassenImage("Up");
                        playPositv();
                    }
                    else
                    {
                        setAufpassenImage("");
                        playNegativ();
                    }
                    if (eanView.getText().toString().equals(MAGGI))
                    {
                        setShowCase("maggi");
                    }
                    if (eanView.getText().toString().equals(GUTFRIED))
                    {
                        setShowCase("gutfried");
                    }
                    if (eanView.getText().toString().equals(SIMPLIV))
                    {
                        setShowCase("simplyv");
                    }
                    Log.d("BUTTONS", "User tapped the Supabutton");
                } catch (Exception e) {
                    CharSequence text = e.getLocalizedMessage();
                    Toast toast = Toast.makeText(binding.getRoot().getContext(),text, Toast.LENGTH_LONG);
                    toast.show();
                    //throw new RuntimeException(e);
                }


            }
        });
        return root;
    }



    private void playPositv()
    {
       /* SoundPool soundPool = getSoundPool();
        int ref = soundPool.load(binding.getRoot().getContext(),loadSoundResssource("positiv"),1);
        soundPool.play(ref, 1.0f, 1.0f, 1, 0, 1.5f);*/
        final MediaPlayer mp = MediaPlayer.create (binding.getRoot().getContext(), loadSoundResssource("positiv"));
        mp.start();


    }
    private void playNegativ()
    {
        /*SoundPool soundPool = getSoundPool();
        int ref = soundPool.load(binding.getRoot().getContext(),loadSoundResssource("negativ"),1);
        soundPool.play(ref, 1.0f, 1.0f, 1, 0, 1.5f);*/
        final MediaPlayer mp = MediaPlayer.create (binding.getRoot().getContext(), loadSoundResssource("negativ"));
        mp.start();
    }

    private void playNeutral()
    {
     /*   SoundPool soundPool = getSoundPool();
        int ref = soundPool.load(binding.getRoot().getContext(),loadSoundResssource("neutral"),1);
        soundPool.play(ref, 1.0f, 1.0f, 1, 0, 1.5f);*/
        final MediaPlayer mp = MediaPlayer.create (binding.getRoot().getContext(), loadSoundResssource("neutral"));
        mp.start();
    }

    private SoundPool getSoundPool()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return  new SoundPool.Builder().setAudioAttributes(getAttributes()).build();

        }
        else
        {
            return new SoundPool(2,AudioManager.STREAM_MUSIC,0);
        }
    }
    private AudioAttributes getAttributes()
    {
        return new AudioAttributes.Builder().setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                .setUsage(AudioAttributes.USAGE_GAME).build();
    }
    private void setAufpassenImage(String imagename)
    {
        final ImageView imgAufpassenView = binding.imgAufpassen;

        if (imagename=="Up")
        {
            //Lade image aus Drawable
            imgAufpassenView.setImageResource(loadResssource("thumb_up_icon_svg"));
        }
        else
        {
            //Lade image aus Drawable
            imgAufpassenView.setImageResource(loadResssource("thumb_down"));
        }
    }

    private void setShowCase(String imagename)
    {
        final ImageView imgShowcaseView = binding.showcase;
        imgShowcaseView.setImageResource(loadResssource(imagename));
    }

    private Integer loadResssource(String name)
    {

        return binding.getRoot().getContext().getResources().getIdentifier(name, "drawable",
                binding.getRoot().getContext().getPackageName());

    }
    private Integer loadSoundResssource(String name)
    {
        return binding.getRoot().getContext().getResources().getIdentifier(name, "raw",
                binding.getRoot().getContext().getPackageName());

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}