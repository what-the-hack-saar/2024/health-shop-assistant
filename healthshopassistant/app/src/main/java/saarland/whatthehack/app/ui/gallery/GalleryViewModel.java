package saarland.whatthehack.app.ui.gallery;

import android.widget.Button;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import saarland.whatthehack.app.R;

public class GalleryViewModel extends ViewModel {

    private final MutableLiveData<String> mText;
    private final MutableLiveData<String> mEanCodeText;
    private final MutableLiveData<String> mZutatenText;
    private final MutableLiveData<String> mProduktname;
    private final MutableLiveData<String> mAufpassen;

    private final MutableLiveData<String> mShowcaseImage;
    private final MutableLiveData<String> mAufpassenImage;



    public GalleryViewModel() {
        mText = new MutableLiveData<>();
        mEanCodeText = new MutableLiveData<>();
        mZutatenText = new MutableLiveData<>();
        mProduktname = new MutableLiveData<>();
        mAufpassen = new MutableLiveData<>();
        mShowcaseImage = new MutableLiveData<>();
        mAufpassenImage = new MutableLiveData<>();
        mEanCodeText.setValue("4005500310105");
        mZutatenText.setValue("Unknown");
        mProduktname.setValue("Unknown");
        mAufpassen.setValue("Nö alles ok");
        mShowcaseImage.setValue("@drawable/stoerbild");
        mAufpassenImage.setValue("@drawable/thumb_down");
        mText.setValue("Here you can see the results");
    }

    public LiveData<String> getText() {
        return mText;
    }
    public LiveData<String> getEanText() {
        return mEanCodeText;
    }

    public LiveData<String> getProduktText() {
        return mProduktname;
    }

    public LiveData<String> getAufpassenText() {
        return mAufpassen;
    }

    public LiveData<String> getZutatenText() {
        return mZutatenText;
    }

}