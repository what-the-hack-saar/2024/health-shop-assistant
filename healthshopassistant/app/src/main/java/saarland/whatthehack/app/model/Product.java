package saarland.whatthehack.app.model;


import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Data;

@Data
public class Product {

    private Images images;

    private Ingredient[] ingredients;
    private LanguagesCodes languagesCodes;

    private NutrientLevels nutrientLevels;

    private Nutriments nutriments;

    private SelectedImages selectedImages;

    private Source[] sources;

    private int additivesN;

    private int additivesOldN;

    private String[] additivesOriginalTags;

    private String[] additivesOldTags;

    private String[] additivesPrevOriginalTags;

    private String[] additivesDebugTags;

    private String[] additivesTags;

    private String allergens;

    private String allergensFromIngredients;

    private String allergensFromUser;

    private String[] allergensHierarchy;

    private String allergensLc;

    private String[] allergensTags;

    private String[] aminoAcidsPrevTags;

    private String[] aminoAcidsTags;

    private String brands;

    private String[] brandsDebugTags;

    private String[] brandsTags;

    private String carbonFootprintPercentOfKnownIngredients;

    private String carbonFootprintFromKnownIngredientsDebug;

    private String categories;

    private String[] categoriesHierarchy;

    private String categoriesLc;

    private String[] categoriesPropertiesTags;

    private String[] categoriesTags;

    private String[] checkersTags;

    private String[] citiesTags;

    private String code;

    private String[] codesTags;

    private String comparedToCategory;

    private int complete;

    private long completedT;

    private float completeness;

    private String conservationConditions;

    private String countries;

    private String[] countriesHierarchy;

    private String countriesLc;

    private String[] countriesDebugTags;

    private String[] countriesTags;

    private String[] correctorsTags;

    private long createdT;

    private String creator;

    private String[] dataQualityBugsTags;

    private String[] dataQualityErrorsTags;

    private String[] dataQualityInfoTags;

    private String[] dataQualityTags;

    private String[] dataQualityWarningsTags;

    private String dataSources;

    private String[] dataSourcesTags;

    private String[] debugParamSortedLangs;

    private String[] editorsTags;

    private String embCodes;

    private String[] embCodesDebugTags;

    private String embCodesOrig;

    private String[] embCodesTags;

    
    private String[] entryDatesTags;

    
    private String expirationDate;

    
    private String[] expirationDateDebugTags;

    
    private int fruitsVegetablesNuts100GEstimate;

    
    private String genericName;

    private String id;

    private String _id;

    
    private String imageFrontSmallUrl;

    
    private String imageFrontThumbUrl;

    
    private String imageFrontUrl;

    
    private String imageIngredientsUrl;

    
    private String imageIngredientsSmallUrl;

    
    private String imageIngredientsThumbUrl;

    
    private String imageNutritionSmallUrl;

    
    private String imageNutritionThumbUrl;

    
    private String imageNutritionUrl;

    
    private String imageSmallUrl;

    
    private String imageThumbUrl;

    
    private String imageUrl;

    
    private String[] informersTags;

    
    private String[] ingredientsAnalysisTags;

    
    private String[] ingredientsDebug;

    
    private int ingredientsFromOrThatMayBeFromPalmOilN;

    
    private String[] ingredientsFromPalmOilTags;

    
    private int ingredientsFromPalmOilN;

    
    private String[] ingredientsHierarchy;

    
    private String[] ingredientsIdsDebug;

    
    private int ingredientsN;

    
    private String[] ingredientsNTags;

    
    private String[] ingredientsOriginalTags;

    
    private String[] ingredientsTags;

    
    private String ingredientsText;

    
    private String ingredientsTextDebug;

    
    private String ingredientsTextWithAllergens;

    
    private int ingredientsThatMayBeFromPalmOilN;

    
    private String[] ingredientsThatMayBeFromPalmOilTags;

    
    private String interfaceVersionCreated;

    
    private String interfaceVersionModified;

    
    private String[] keywords;

    
    private int knownIngredientsN;

    private String labels;

    
    private String[] labelsHierarchy;

    
    private String labelsLc;

    
    private String[] labelsPrevHierarchy;

    
    private String[] labelsPrevTags;

    
    private String[] labelsTags;

    
    private String[] labelsDebugTags;

    private String lang;

    
    private String[] langDebugTags;

    
    private String[] languagesHierarchy;

    
    private String[] languagesTags;

    
    private String[] lastEditDatesTags;

    
    private String lastEditor;

    
    private String[] lastImageDatesTags;

    
    private long lastImageT;

    
    private String lastModifiedBy;

    
    private long lastModifiedT;

    private String lc;

    private String link;

    
    private String[] linkDebugTags;

    
    private String manufacturingPlaces;

    
    private String[] manufacturingPlacesDebugTags;

    
    private String[] manufacturingPlacesTags;

    
    private String maxImgid;

    
    private String[] mineralsPrevTags;

    
    private String[] mineralsTags;

    
    private String[] miscTags;

    
    private String netWeightUnit;

    
    private String netWeightValue;

    
    private String nutritionDataPer;

    
    private int nutritionScoreWarningNoFruitsVegetablesNuts;

    
    private String noNutritionData;

    
    private String novaGroup;

    
    private String novaGroups;

    
    private String novaGroupDebug;

    
    private String[] novaGroupTags;

    
    private String[] novaGroupsTags;

    
    private String[] nucleotidesPrevTags;

    
    private String[] nucleotidesTags;

    
    private String[] nutrientLevelsTags;

    
    private String nutritionData;

    
    private String[] nutritionDataPerDebugTags;

    
    private String nutritionDataPrepared;

    
    private String nutritionDataPreparedPer;

    
    private String nutritionGrades;

    
    private int nutritionScoreBeverage;

    
    private String nutritionScoreDebug;

    
    private int nutritionScoreWarningNoFiber;

    
    private String[] nutritionGradesTags;

    private String origins;

    
    private String[] originsDebugTags;

    
    private String[] originsTags;

    
    private String otherInformation;

    
    private String[] otherNutritionalSubstancesTags;

    private String packaging;

    
    private String[] packagingDebugTags;

    
    private String[] packagingTags;

    
    private String[] photographersTags;

    
    private String pnnsGroups1;

    
    private String pnnsGroups2;

    
    private String[] pnnsGroups1Tags;

    
    private String[] pnnsGroups2Tags;

    
    private long popularityKey;

    
    private String producerVersionId;

    
    private String productName;

    
    private String productQuantity;

    
    private String purchasePlaces;

    
    private String[] purchasePlacesDebugTags;

    
    private String[] purchasePlacesTags;

    
    private String[] qualityTags;

    private String quantity;

    
    private String[] quantityDebugTags;

    
    private String recyclingInstructionsToDiscard;

    private int rev;

    
    private String servingQuantity;

    
    private String servingSize;

    
    private String[] servingSizeDebugTags;

    private long sortkey;

    private String states;

    
    private String[] statesHierarchy;

    
    private String[] statesTags;

    private String stores;

    
    private String[] storesDebugTags;

    
    private String[] storesTags;

    private String traces;

    
    private String tracesFromIngredients;

    
    private String[] tracesHierarchy;

    
    private String[] tracesDebugTags;

    
    private String tracesFromUser;

    
    private String tracesLc;

    
    private String[] tracesTags;

    
    private int unknownIngredientsN;

    
    private String[] unknownNutrientsTags;

    
    private String updateKey;

    
    private String[] vitaminsPrevTags;

    
    private String[] vitaminsTags;

    Map<String, Object> other = new LinkedHashMap<>();

   
    void setDetail(String key, Object value) {
        other.put(key, value);
    }
}
