package saarland.whatthehack.app.model;

import lombok.Data;


@Data
public class SelectedImageItem {

    private String en;

    private String fr;

    private String pl;

    public String getUrl() { if (en!=null) return en;
        if(fr!=null) return fr;
        if (pl!=null) return pl;
        return null;
    }
}
