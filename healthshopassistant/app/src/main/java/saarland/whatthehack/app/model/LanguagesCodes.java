package saarland.whatthehack.app.model;

import lombok.Data;

@Data
public class LanguagesCodes {

    private String en;

    private String fr;

    private String pl;
}
