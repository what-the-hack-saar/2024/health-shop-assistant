package saarland.whatthehack.app.model;

import lombok.Data;

@Data
public class NutrientLevels {

    private String fat;

    private String salt;

    private String saturatedFat;

    private String sugars;
}
