package saarland.whatthehack.app.client;

import android.os.StrictMode;

import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.net.HttpHeaders;
import com.google.firebase.crashlytics.buildtools.reloc.com.google.common.net.MediaType;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.HttpResponse;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.HttpClient;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.methods.HttpGet;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.methods.HttpUriRequest;
import com.google.gson.Gson;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import saarland.whatthehack.app.model.ProductResponse;


public class OpenFoodFactsApiLowLevelClient {

  private static final String API_URL = "https://world.openfoodfacts.org/api/v0";


  public OpenFoodFactsApiLowLevelClient() throws Exception {

  }

  public ProductResponse fetchProductByCode(String code) throws IOException, JSONException {
    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
            .permitAll().build();
    StrictMode.setThreadPolicy(policy);
    URL url = new URL(String.format("%s/product/%s.json", API_URL, code));
    HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

    try {
      InputStream in = new BufferedInputStream(urlConnection.getInputStream());
      StringBuilder sb = new StringBuilder();
      for (int ch; (ch = in.read()) != -1; ) {
        sb.append((char) ch);
      }
      String json = sb.toString();
      if (json!=null)
      {
        Gson gson = new Gson();
        return gson.fromJson(json, ProductResponse.class);
      }
      else {
        return null;
      }
    } finally {
      urlConnection.disconnect();
    }

  }
}
