package saarland.whatthehack.app.model;


import lombok.Data;

@Data
public class ProductResponse {

    private Product product;

    private String code;

    private String status;

    
    private String statusVerbose;
}
