package saarland.whatthehack.app.model;


import java.util.LinkedHashMap;
import java.util.Map;
import lombok.Data;


@Data
public class Images {

    Map<String, Object> other = new LinkedHashMap<>();

    void setDetail(String key, Object value) {
        other.put(key, value);
    }
}
