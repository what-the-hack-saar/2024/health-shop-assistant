package saarland.whatthehack.app.model;

import lombok.Data;

@Data
public class Ingredient {

    private String fromPalmOil;

    private String id;

    private String origin;

    private String percent;

    private int rank;

    private String text;

    private String vegan;

    private String vegetarian;
}
