package saarland.whatthehack.app.model;


import lombok.Data;

@Data
public class Source {

    private String[] fields;

    private String id;

    private String[] images;

    
    private long importT;

    private String manufacturer;

    private String name;

    private String url;
}
